# com_arduino_max
# Comunicación serial entre Max y Arduino.

Presento dos programas de Arduino y un patcher de Max donde podrán ver cómo se comunican estos dos programas de manera rápida y sencilla.

Un tip muy importante: debemos abrir los programas en este orden para que todo funcione:

1. Cerrar tanto Arduino como Max
2. Abrir Arduino
3. Abrir el monitor serial de Arduino
4. Cerrar el monitor serial de Arduino
5. Abrir Max y comenzar a enviar o recibir
